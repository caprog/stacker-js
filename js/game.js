const canvas = document.getElementById('game')

const SQUARE_IMG = new Image()
SQUARE_IMG.src = 'assets/sprites/square.png'

const FILLED_SQUARE_IMG = new Image()
FILLED_SQUARE_IMG.src = 'assets/sprites/filled_square.png'

const SOUNDS = {
    start : new Audio('assets/sound/start.ogg'),
    lose : new Audio('assets/sound/lose.ogg'),
    hit : new Audio('assets/sound/hit.ogg'),
    win : new Audio('assets/sound/win.ogg'),
}

const CTX = canvas.getContext('2d')

let imgWidth = 32
let imgHeight = 32

const SPEED = 60

let enterPressed = false
let showLose = false
let showWin = false
let showStart = true
let grid = []
let rectangle = {}
let touchScreen = false

checkEnterInput()
setInterval(() => {
    resizeGame()
    clearContext2D()
    updateRectangle()
    drawGrid()
    drawRectangle()
    drawWindowLose()
    drawWindowWin()
    drawWindowStart()
}, SPEED)

function resizeGame() {
    const body = document.body 
    if(canvas.width !== body.clientWidth) {
        imgHeight = body.clientHeight / 15 
        imgWidth = imgHeight
        canvas.width = imgWidth * 7
        canvas.height = body.clientHeight
    }
}

function checkEnterInput() {
    document.addEventListener('keydown', function(evt) {
        if(evt.key === 'Enter' && !enterPressed) {
            performAction()
        }

    })
    document.addEventListener('keyup', function(evt) {
        if(evt.key === 'Enter') enterPressed = false
    })
    document.addEventListener('mousedown', function(evt) {
        performAction()
    })
}

function performAction() {
    if (showStart === true || showLose === true || showWin === true) {
        restartGame()
    } else {
        printRectangle()
        removeSquares()
        jumpRectangle()
        checkLose()
        checkWin()
    }
    enterPressed = true
}

function drawWindowStart() {
    if(showStart === true) printWindows('Stacker', '- press enter or click to start -')
}

function drawWindowWin() {
    if(showWin === true) printWindows('You win!', '- press enter or click to restart -')
}

function printWindows(title, subtitle) {
    const hCenterCanvas = canvas.width / 2
    const vCenterCanvas = canvas.height / 2
    CTX.fillStyle = 'black'
    CTX.globalAlpha = 0.9
    CTX.fillRect(0, 0, canvas.width, canvas.height)
    CTX.globalAlpha = 1
    CTX.fillStyle = 'white'
    CTX.font = `${imgWidth * 0.7}px Arial`
    printCenterText(title, hCenterCanvas, vCenterCanvas * 0.9)

    CTX.font = `${imgWidth * 0.45}px Arial`
    printCenterText(subtitle, hCenterCanvas, vCenterCanvas)
}

function checkWin() {
    if(rectangle.currentRow === -1 && rectangle.squares > 0)  {
        showWin = true
        SOUNDS.win.play()
    }
}

function restartGame() {
    SOUNDS.start.play()
    showLose = false
    showWin = false
    showStart = false
    rectangle = {
        currentRow: 14,
        currentCol: 0,
        squares: 3,
        move: 'right'
    }
    grid = [
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
    ]
}

function drawWindowLose() {
    if(showLose === true) printWindows('You lose...', '- press enter or click to restart -')
}

function printCenterText(text, x, y) {
    const CENTER_TEXT = x - CTX.measureText(text).width / 2
    CTX.fillText(text, CENTER_TEXT, y)
}

function removeSquares() {
    let removeSquares = 0
    for (let squareNumber = 0; squareNumber < rectangle.squares; squareNumber++) {
        const COL_NUMBER = rectangle.currentCol + squareNumber
        if(rectangle.currentRow < grid.length - 1 && 
            grid[rectangle.currentRow + 1][COL_NUMBER] === 0)
                removeSquares += 1
        
        if(COL_NUMBER < 0 || COL_NUMBER > 6)
            removeSquares += 1
    }
    rectangle.squares -= removeSquares
}

function jumpRectangle() {
    rectangle.currentRow -= 1
    if(rectangle.currentCol < 0) {
        rectangle.currentCol = 0
        rectangle.move = 'right'
    }
}

function checkLose() {
    if(rectangle.squares === 0) {
        showLose = true
        SOUNDS.lose.play()
    } else SOUNDS.hit.play()
}

function printRectangle() {
    for (let squareNumber = 0; squareNumber < rectangle.squares; squareNumber++) {
        grid[rectangle.currentRow][rectangle.currentCol + squareNumber] = 1
    }
}

function clearContext2D() {
    CTX.clearRect(0, 0, canvas.width, canvas.height)
}

function updateRectangle() {
    if(rectangle.currentCol === 6) rectangle.move = 'left'
    if(rectangle.currentCol === 1 - rectangle.squares) rectangle.move = 'right'
    if(rectangle.move === 'right') rectangle.currentCol += 1
    if(rectangle.move === 'left') rectangle.currentCol -= 1
}

function drawRectangle() {
    for (let squareNumber = 0; squareNumber < rectangle.squares; squareNumber++) {
        CTX.drawImage(FILLED_SQUARE_IMG, (rectangle.currentCol + squareNumber) * imgWidth, rectangle.currentRow * imgHeight, imgWidth, imgHeight)
    }
}

function drawGrid() {
    for (let rowNumber = 0; rowNumber < grid.length; rowNumber++) {
        const row = grid[rowNumber]
        for (let colNumber = 0; colNumber < row.length; colNumber++) {
            const value = row[colNumber]
            if (value === 0) {
                CTX.drawImage(SQUARE_IMG, colNumber * imgWidth, rowNumber * imgHeight, imgWidth, imgHeight)
            }
            if (value === 1) {
                CTX.drawImage(FILLED_SQUARE_IMG, colNumber * imgWidth, rowNumber * imgHeight, imgWidth, imgHeight)
            }
        }
    }
}
